package net.welen.jmole.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.welen.jmole.Lifecycle;

public class LifeCycleServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;	

	@Override
	public void init() throws ServletException {
		Lifecycle.setup();
	}

	@Override
	public void destroy() {
		Lifecycle.cleanup();
	}
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Do nothing
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Do nothing
	}

}
