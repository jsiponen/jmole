package net.welen.jmole;

public class Mock implements MockMBean {

	String attribute = "";

	public String getAttribute() {
		return attribute;
	}

	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}

}
