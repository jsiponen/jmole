package net.welen.jmole;

import javax.management.MBeanServer; 
import javax.management.ObjectName;
import javax.management.InstanceAlreadyExistsException;
import javax.management.InstanceNotFoundException;

import org.junit.Test;
import org.junit.Before;
import static org.junit.Assert.*;

public class JMoleTest {

	private MBeanServer server = Utils.getMBeanServer(); 

	@Before
	public void setup() {
		System.setProperty(JMole.CONFIG_FILENAMES_PROPERTY, "src/test/resources/JMole_Test.xml");
	}

	@Test	
	public void testMBeanRegistration() throws Exception {
		ObjectName objectName = new ObjectName(JMole.OBJECT_NAME);
		JMole jmole = new JMole();
		assertFalse(server.isRegistered(objectName));
		jmole.register();
		assertTrue(server.isRegistered(objectName));
		jmole.unregister();
		assertFalse(server.isRegistered(objectName));
	}

	@Test
	public void testMultipleMBeanRegistration() throws Exception {
		JMole jmole = new JMole();
		jmole.register();
		try {
			jmole.register();
		} catch (InstanceAlreadyExistsException e){
			// Do nothing. Expected
		} finally {
			jmole.unregister();
		}
	}


	@Test(expected=InstanceNotFoundException.class)
	public void testUnregistrationWithoutAnyMBean() throws Exception {
		new JMole().unregister();
	}

	@Test
	public void testConfiguration() throws Exception {
		JMole jmole = new JMole();
		assertEquals(0, jmole.getConfiguration().size());
		jmole.configure();
		assertEquals(1, jmole.getConfiguration().size());
	}

	@Test
	public void testDiscovery() throws Exception {
		JMole jmole = new JMole();
		jmole.register();
		jmole.configure();
		assertEquals(1, jmole.getConfiguration().size());
		assertEquals(0, jmole.getConfiguration().iterator().next().getMBeanFinder().getMatchingObjectNames().size());

		jmole.discover();
		assertEquals(0, jmole.getConfiguration().iterator().next().getMBeanFinder().getMatchingObjectNames().size());

		ObjectName objectName = new ObjectName("net.welen.jmole.test:test=1");
		Mock mock = new Mock();
		mock.setAttribute("TestString");
                server.registerMBean(mock, objectName);
		assertEquals(1, jmole.getConfiguration().iterator().next().getMBeanFinder().getMatchingObjectNames().size());
                server.unregisterMBean(objectName);

		jmole.unregister();
	}

	@Test
	public void testCollectingValues() throws Exception {
		JMole jmole = new JMole();
                jmole.register();
		jmole.configure();

		ObjectName objectName = new ObjectName("net.welen.jmole.test:test=1");
                Mock mock = new Mock();
                mock.setAttribute("TestString");
		server.registerMBean(mock, objectName);

		assertEquals("Name TestString", jmole.getConfiguration().iterator().next().getMBeanCollector().getName(objectName));
		assertEquals("TestString", jmole.getConfiguration().iterator().next().getMBeanCollector().getValues(objectName).get("Attribute"));

                server.unregisterMBean(objectName);

		jmole.unregister();
	}

}	
