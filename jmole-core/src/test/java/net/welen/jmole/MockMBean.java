package net.welen.jmole;

public interface MockMBean {

	public String getAttribute();

	public void setAttribute(String attribute);

}
