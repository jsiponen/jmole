package net.welen.jmole;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import net.welen.jmole.protocols.Protocol;
import net.welen.jmole.protocols.munin.Munin;
import net.welen.jmole.protocols.nrpe.NRPE;

@Singleton
@Startup
public class Lifecycle {

	private final static Logger LOG = Logger.getLogger(Lifecycle.class.getName());
	
	private static JMole jmole;	
	private static Protocol[] protocols = { new Munin(), new NRPE() };
	private static boolean running = false; 

	@PostConstruct
	public static synchronized void setup() {
		if (running) {
			return;
		}
		running = true;
		
		LOG.log(Level.INFO, "Starting JMole");
		try {
			jmole = new JMole();
			jmole.register();
			jmole.configure();

			// Start protocols
			for (Protocol protocol : protocols) {
				if (protocol.isEnabled()) {				
					protocol.startProtocol(jmole);
				}
			}
				
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@PreDestroy
	public static void cleanup() {
		LOG.log(Level.INFO, "Stopping JMole");
		try {
			// Stop protocols
			for (Protocol protocol : protocols) {
				if (protocol.isEnabled()) {				
					protocol.stopProtocol();
				}
			}						
			jmole.unregister();
			running = false;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
