package net.welen.jmole;

import javax.management.MalformedObjectNameException;

import java.util.List;
import java.util.Map;
import java.io.FileNotFoundException;

import javax.management.MBeanException;
import javax.management.AttributeNotFoundException;
import javax.management.InstanceNotFoundException;
import javax.management.ReflectionException;
import javax.management.IntrospectionException;

public interface JMoleMBean {

	public void configure() throws MalformedObjectNameException,
			FileNotFoundException, MBeanException, AttributeNotFoundException,
			InstanceNotFoundException, ReflectionException,
			IntrospectionException;

	public void discover() throws MBeanException, AttributeNotFoundException,
			InstanceNotFoundException, ReflectionException,
			IntrospectionException;

	public Map<String, List<Map<String, Map<String, Object>>>> collectMeasurements()
			throws InstanceNotFoundException, ReflectionException,
			MBeanException, AttributeNotFoundException;

	public String collectMeasurementsAsJSON()
			throws InstanceNotFoundException, ReflectionException,
			MBeanException, AttributeNotFoundException;

	public Map<String, Map<String, Object>> collectMeasurements(String category)
			throws InstanceNotFoundException, ReflectionException,
			MBeanException, AttributeNotFoundException;

	public String collectMeasurementsAsJSON(String category)
			throws InstanceNotFoundException, ReflectionException,
			MBeanException, AttributeNotFoundException;

	public Map<String, Object> collectMeasurements(String category, String name)
			throws InstanceNotFoundException, ReflectionException,
			MBeanException, AttributeNotFoundException;

	public String collectMeasurementsAsJSON(String category, String name)
			throws InstanceNotFoundException, ReflectionException,
			MBeanException, AttributeNotFoundException;

	public Object collectMeasurement(String category, String name, String attribute)
			throws InstanceNotFoundException, ReflectionException,
			MBeanException, AttributeNotFoundException;

	public String collectMeasurementAsJSON(String category, String name, String attribute)
			throws InstanceNotFoundException, ReflectionException,
			MBeanException, AttributeNotFoundException;

	public Map<String, Map<String, String>> warningMessages() throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException;
	public String warningMessagesAsJSON() throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException;
	
	public Map<String, Map<String, String>> criticalMessages() throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException;
	public String criticalMessagesAsJSON() throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException;
	
}
