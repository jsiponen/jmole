package net.welen.jmole.finder;

import java.util.List;
import java.util.Set;

import javax.management.ObjectName;
import javax.management.MBeanException;
import javax.management.AttributeNotFoundException;
import javax.management.InstanceNotFoundException;
import javax.management.ReflectionException;
import javax.management.IntrospectionException;
import javax.management.MalformedObjectNameException;

public interface MBeanFinder {
	
	public Set<ObjectName> getMatchingObjectNames();
	
	public void updateMatchingObjectNames() throws MBeanException,
			AttributeNotFoundException, InstanceNotFoundException,
			ReflectionException, IntrospectionException;
	
	public void setObjectNameQueries(List<String> objectNameQueries) throws MalformedObjectNameException;
	
	public void setAttributeName(String attributeName);

	public void setAttributeMatch(String attributeMatch);

	public void setParameterName(String parameterName);

	public void setParameterMatch(String parameterMatch);

	public void setClassName(String className);
}