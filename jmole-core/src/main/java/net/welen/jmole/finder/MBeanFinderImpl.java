package net.welen.jmole.finder;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.management.ObjectName;
import javax.management.MBeanServer;
import javax.management.MBeanException;
import javax.management.AttributeNotFoundException;
import javax.management.InstanceNotFoundException;
import javax.management.ReflectionException;
import javax.management.IntrospectionException;

import javax.management.MalformedObjectNameException;

import net.welen.jmole.Utils;

public class MBeanFinderImpl implements MBeanFinder {
	
	private final static Logger LOG = Logger.getLogger(MBeanFinderImpl.class.getName());
	
	private MBeanServer server = Utils.getMBeanServer();

	private Set<ObjectName> matchingObjectNames = new HashSet<ObjectName>();	
	private List<ObjectName> objectNameQueries = new ArrayList<ObjectName>();
	private String attributeName = null;
	private String attributeMatch = null;
	private String parameterName = null;
	private String parameterMatch = null;
	private String className = null;

	public Set<ObjectName> getMatchingObjectNames() {
		return matchingObjectNames;
	}

	public void updateMatchingObjectNames() throws MBeanException,
			AttributeNotFoundException, InstanceNotFoundException,
			ReflectionException, IntrospectionException {
				
		Set<ObjectName> objectNames = new HashSet<ObjectName>();
		for (ObjectName query : objectNameQueries) {
			objectNames.addAll(server.queryNames(query, null));
		}
		LOG.log(Level.FINE, "Found ObjectNames: " + objectNames);

		Set<ObjectName> filteredObjectNames = new HashSet<ObjectName>();
		for (ObjectName objectName : objectNames) {
			LOG.log(Level.FINE, "Checking ObjectName: " + objectName);
			if (!attributeMatch(objectName)) {
				LOG.log(Level.FINE, "Removing ObjectName: " + objectName);
				continue;
			}			
			if (!parameterMatch(objectName)) {
				LOG.log(Level.FINE, "Removing ObjectName: " + objectName);
				continue;
			}			
			if (!classNameCheck(objectName)) {
				LOG.log(Level.FINE, "Removing ObjectName: " + objectName);
				continue;
			}
			filteredObjectNames.add(objectName);
		}
		LOG.log(Level.FINE, "Updated ObjectNames: " + filteredObjectNames);
		matchingObjectNames = filteredObjectNames;
	}

	public void setObjectNameQueries(List<String> objectNameQuerys)
			throws MalformedObjectNameException {
		this.objectNameQueries = new ArrayList<ObjectName>();
		for (String query : objectNameQuerys) {		
			this.objectNameQueries.add(new ObjectName(query));
		}
	}

	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}

	public void setAttributeMatch(String attributeMatch) {
		this.attributeMatch = attributeMatch;
	}

	public void setParameterName(String parameterName) {
		this.parameterName = parameterName;
	}

	public void setParameterMatch(String parameterMatch) {
		this.parameterMatch = parameterMatch;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	private boolean attributeMatch(ObjectName objectName)
			throws MBeanException, InstanceNotFoundException, ReflectionException {

		if (attributeName == null || attributeMatch == null) {
			return true;
		}

		try  {
			Object attribute = server.getAttribute(objectName, attributeName);
			if (attribute instanceof String) {
				return attribute.toString().matches(attributeMatch);
			}
		} catch (AttributeNotFoundException e) {
			return false;
		}
		return true;
	}

	private boolean parameterMatch(ObjectName objectName)
			throws MBeanException, InstanceNotFoundException, ReflectionException {

		if (parameterName == null || parameterMatch == null) {
			return true;
		}

		String parameter = objectName.getKeyProperty(parameterName);	
		return parameter.toString().matches(parameterMatch);
	}

	private boolean classNameCheck(ObjectName objectName)
			throws MBeanException, AttributeNotFoundException,
			InstanceNotFoundException, ReflectionException,
			IntrospectionException {

		if (className == null) {
			return true;
		}

		return server.getMBeanInfo(objectName).getClassName().equals(className);
	}
}
