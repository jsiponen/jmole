package net.welen.jmole.collector.extractor_impl;

import javax.management.openmbean.CompositeData;

import net.welen.jmole.collector.DataCollectorExtractor;


public class CompositeDataExtractor implements DataCollectorExtractor {

	private String fakeAttribute = null;
	private String realAttribute = null;
	
	public void setFakeAttribute(String attribute) {
		this.fakeAttribute = attribute;
	}
	
	public String getFakeAttribute() {
		return fakeAttribute;
	}
	
	public void setRealAttribute(String attribute) {
		this.realAttribute = attribute;
	}
	
	public String getRealAttribute() {
		return realAttribute;
	}
	
	public Object extractData(Object dataSource) {
		if (dataSource instanceof CompositeData) {
			CompositeData compositeData = (CompositeData) dataSource;
			return compositeData.get(fakeAttribute);
		} else {
			throw new IllegalArgumentException("Illegal class");
		}
	}
}
