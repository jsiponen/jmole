package net.welen.jmole.collector;

public interface DataCollectorExtractor {

	public void setRealAttribute(String attribute);
	public String getRealAttribute();
	public String getFakeAttribute();
	public Object extractData(Object dataSource);
}
