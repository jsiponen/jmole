package net.welen.jmole.collector;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.management.MBeanServer;

import javax.management.ObjectName;
import javax.management.MBeanException;
import javax.management.AttributeNotFoundException;
import javax.management.InstanceNotFoundException;
import javax.management.ReflectionException;
import javax.management.Attribute;
import javax.management.AttributeList;

import net.welen.jmole.Utils;


public class MBeanCollectorImpl implements MBeanCollector {
	private final static Logger LOG = Logger.getLogger(MBeanCollectorImpl.class.getName());
	
	private MBeanServer server = Utils.getMBeanServer();

	private String name = "%s";
	private List<String> nameAttributes = null;	
	private List<String> nameParameters = null;

	private Map<String, Integer> counterIntervals = new HashMap<String, Integer>();
	private Map<String, LastFetch> lastFetchMap = new HashMap<String, LastFetch>();
	
	private List<String> attributes = new ArrayList<String>();
	private Map<String, DataCollectorExtractor> attributeExtractors = new HashMap<String, DataCollectorExtractor>();

	private class LastFetch {
		public LastFetch(long fetchTime, Double value, Double lastCalculatedValue) {
			this.fetchTime = fetchTime;
			this.value = value;
			this.lastCalculatedValue = lastCalculatedValue;
		}
		long fetchTime;
		Double value;
		Double lastCalculatedValue;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}

	public void setNameAttributes(List<String> nameAttributes) {
		this.nameAttributes = nameAttributes;
	}	

	public void setNameParameters(List<String> nameParameters) {
		this.nameParameters = nameParameters;
	}

	public Map<String, Integer> getCounterIntervals() {
		return counterIntervals;
	}

	public void setCounterIntervals(Map<String, Integer> counterIntervals) {
		this.counterIntervals = counterIntervals;
	}

	private String getNameSubstitution(ObjectName objectName) throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException {
		if (nameAttributes != null) {
			StringBuilder tmp = new StringBuilder();
			for (String part : nameAttributes) {
				if (tmp.length() > 0) {
					tmp.append(" ");
				}
				tmp.append(server.getAttribute(objectName, part).toString());	
			}
			return tmp.toString();
		}
		if (nameParameters != null) {
			StringBuilder tmp = new StringBuilder();
			for (String part : nameParameters) {
				if (tmp.length() > 0) {
					tmp.append(" ");
				}
				tmp.append(objectName.getKeyProperty(part));	
			}
			return tmp.toString();
		}
		return null;
	}
	
	public void setAttributes(List<String> attributes) {
		this.attributes = attributes;
	}
	
	public List<String> getAttributes() {
		return attributes;
	}

	public void setDataCollectorExtractors(Map<String, DataCollectorExtractor> attributeExtractors) {
		this.attributeExtractors = attributeExtractors;
	}

	public String getName(ObjectName objectName) throws MBeanException,
			AttributeNotFoundException, InstanceNotFoundException,
			ReflectionException {
		
		String substition = getNameSubstitution(objectName);
		if (substition != null) {
			return String.format(name, substition);
		}
		return name;
	}
	
	public Map<String, Object> getValues(ObjectName objectName) throws InstanceNotFoundException, ReflectionException {
		LOG.log(Level.FINE, "Getting the values for ObjectName: " + objectName);

		List<String> attributesToFetch = new ArrayList<String>();
		attributesToFetch.addAll(attributes);
		LOG.log(Level.FINE, "Unfixed attribute list: " + attributesToFetch);
		for (DataCollectorExtractor extractor : attributeExtractors.values()) {
			try {
				attributesToFetch.add(extractor.getRealAttribute());
			} catch (Exception e) {
				LOG.log(Level.SEVERE, e.getMessage(), e);
			}
		}
		attributesToFetch.removeAll(attributeExtractors.keySet());		
		
		LOG.log(Level.FINE, "Fixed attribute list: " + attributesToFetch);		
		AttributeList values = server.getAttributes(objectName, attributesToFetch.toArray(new String[0]));

		// Quick fix #1 for old JBoss MBeanServer impl. that return to many attributes/values from getAttributes()
		if (values.size() > attributesToFetch.size()) {
			LOG.log(Level.FINE, "Fixing JBoss MBean server bug. Got: " + values + " expected only: " + attributesToFetch);			
			AttributeList fixed = new AttributeList();
			for (Attribute value : values.asList()) {
				if (attributesToFetch.contains(value.getName())) {
					fixed.add(value);
				}
			}
			values = fixed;
		}		
		
		// Quick fix #2 for old JBoss MBeanServer impl. where getAttributes() doesn't seem to return super class attributes
		if (values.size() != attributesToFetch.size()) {
			for (String attribute : attributesToFetch) {
				if (!values.contains(attribute)) {
					try {
						values.add(new Attribute(attribute, server.getAttribute(objectName, attribute)));
						LOG.log(Level.FINE, "Fixing JBoss MBean server bug for MBean: " + objectName + " and attribute: " + attribute);					
					} catch (Exception e) {
						// Do nothing, just continue
					}
				}
			}
		}
		
		if (values.size() != attributesToFetch.size()) {
			LOG.log(Level.WARNING, "All specified attributes is not available from MBean: " + objectName + ". Expected: " 
							+ attributesToFetch + ", Found: " + values + " Total: " + server.getAttributes(objectName, new String[0]));		
		}

		Map<String, Object> answer = new HashMap<String, Object>();
		
		for (Attribute attribute : values.asList()) {
			String attributeName = attribute.getName();
			Object value = attribute.getValue();

			LOG.log(Level.FINE, "Getting the attribute : " + attributeName);
			
			// Check if we have a counter interval and calculate the value instead
			if (counterIntervals.containsKey(attributeName)) {			
				value = calculateCounterValue(objectName + "->"+ attributeName, Double.parseDouble(value.toString()), counterIntervals.get(attributeName));
			}
			if (value == null) {
				LOG.log(Level.FINE, "Attribute: " + attributeName + " is null for ObjectName: " + objectName);
			}
			
			boolean found = false;
			for (DataCollectorExtractor extractor : attributeExtractors.values()) {
				if (attributeName.equals(extractor.getRealAttribute())) {
					answer.put(extractor.getFakeAttribute(), extractor.extractData(value));
					found = true;
				}
			}				
			if (!found) {					
				LOG.log(Level.FINE, "Attribute: " + attributeName + " is " + value + " for ObjectName: " + objectName);
				answer.put(attributeName, value);
			}
		}		
	
		LOG.log(Level.FINE, "Returning: " + answer);			
		return answer;
	}
	
	private Double calculateCounterValue(String key, Double value, int counterInterval) {
		LOG.log(Level.FINE, "Checking " + key);

		long now = System.currentTimeMillis();
		LastFetch lastFetch = lastFetchMap.get(key);		
		
		if (lastFetch == null) {			
			LOG.log(Level.FINE, "No earlier fetch available, skipping");
			lastFetchMap.put(key, new LastFetch(now, value, null));
			return null;
		}		
		if (lastFetch.value > value) {			
			LOG.log(Level.FINE, "Skipping and reset due to that " + lastFetch.value + " > " + value);
			lastFetchMap.put(key, new LastFetch(now, value, null));
			return null;
		}		
		if (lastFetch.fetchTime + counterInterval > now) {
			LOG.log(Level.FINE, "Returning last know calculated value due to that the counter interval " + counterInterval + " ms is not reached: " + (lastFetch.fetchTime + counterInterval) + " > " +  now);
			return lastFetch.lastCalculatedValue;
		}

		// Calculate the value
		Double calculatedValue = (value - lastFetch.value) / ( (now - lastFetch.fetchTime) / counterInterval );

		// Save fetchTime and value
		lastFetchMap.put(key, new LastFetch(now, value, calculatedValue));
				
		LOG.log(Level.FINE, "Returning calculated value: " + calculatedValue);
		return calculatedValue;
	}
	
}
