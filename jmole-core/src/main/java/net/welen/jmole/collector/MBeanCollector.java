package net.welen.jmole.collector;

import java.util.List;
import java.util.Map;

import javax.management.ObjectName;
import javax.management.MBeanException;
import javax.management.AttributeNotFoundException;
import javax.management.InstanceNotFoundException;
import javax.management.ReflectionException;

public interface MBeanCollector {
	
	public void setName(String name);	
	public String getName();
	
	public void setNameAttributes(List<String> nameAttributes);
	public void setNameParameters(List<String> nameParameters);
	
	public void setAttributes(List<String> attributes);
	public List<String> getAttributes();
	public void setDataCollectorExtractors(Map<String, DataCollectorExtractor> attributeExtractors);
	
	public String getName(ObjectName objectName) throws MBeanException,
			AttributeNotFoundException, InstanceNotFoundException,
			ReflectionException;

	public Map<String, Object> getValues(ObjectName objectName) throws InstanceNotFoundException, ReflectionException;

}