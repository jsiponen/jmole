package net.welen.jmole;

import java.util.HashMap;
import java.util.Map;

import net.welen.jmole.collector.MBeanCollector;
import net.welen.jmole.finder.MBeanFinder;
import net.welen.jmole.presentation.PresentationInformation;
import net.welen.jmole.threshold.Threshold;

public class Configuration {
	
	private MBeanFinder mbeanFinder = null;
	private MBeanCollector mbeanCollector = null;
	private PresentationInformation presentationInformation = null;
	private Map<String, Threshold> thresholds = new HashMap<String, Threshold>();
	private int level = 3;

	public MBeanFinder getMBeanFinder() {
		return mbeanFinder;
	}

	public void setMBeanFinder(MBeanFinder mbeanFinder) {
		this.mbeanFinder = mbeanFinder;
	}

	public MBeanCollector getMBeanCollector() {
		return mbeanCollector;
	}

	public void setMBeanCollector(MBeanCollector mbeanCollector) {
		this.mbeanCollector = mbeanCollector;
	}

	public PresentationInformation getPresentationInformation() {
		return presentationInformation;
	}

	public void setPresentationInformation(PresentationInformation presentationInformation) {
		this.presentationInformation = presentationInformation;
	}

	public void setThresholds(Map<String, Threshold> thresholds) {
		this.thresholds = thresholds;
	}
	
	public Map<String, Threshold> getThresholds() {
		return thresholds;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		if (level < 1 || level > 5) {
			throw new RuntimeException(JMole.CONFIG_LEVEL_PROPERTY + " must be 1-5");
		}
		this.level = level;
	}

}
