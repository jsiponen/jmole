package net.welen.jmole.presentation;

import java.util.Map;
import java.util.HashMap;

public class PresentationInformation {

	private String category = null;
	private String description = null;
	private String unit = null;
	private Map<String, String> attributeLabels = new HashMap<String, String>();
	private Map<String, String> attributeDescriptions = new HashMap<String, String>();

	public void setCategory(String category) {
		this.category = category;
	}

	public String getCategory() {
		return category;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getUnit() {
		return unit;
	}

	public void setAttributeLabels(Map<String, String> attributeLabels) {
		this.attributeLabels = attributeLabels;
	}

	public String translateAttributeLabel(String attributeName) {
		String label = attributeLabels.get(attributeName);
		if (label != null) {
			return label;
		}
		return attributeName;
	}

	public void setAttributeDescriptions(Map<String, String> attributeDescriptions) {
		this.attributeDescriptions = attributeDescriptions;
	}

	public String getAttributeDescription(String attributeName) {
		return attributeDescriptions.get(attributeName);
        }

}
