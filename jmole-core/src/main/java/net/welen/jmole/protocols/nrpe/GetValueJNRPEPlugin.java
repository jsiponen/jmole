package net.welen.jmole.protocols.nrpe;

import java.util.logging.Level;
import java.util.logging.Logger;

import net.welen.jmole.JMole;
import it.jnrpe.ICommandLine;
import it.jnrpe.ReturnValue;
import it.jnrpe.Status;
import it.jnrpe.plugins.IPluginInterface;
import it.jnrpe.utils.BadThresholdException;
import it.jnrpe.utils.ThresholdUtil;

public class GetValueJNRPEPlugin implements IPluginInterface {

	private final static Logger LOG = Logger
			.getLogger(GetValueJNRPEPlugin.class.getName());

	private JMole jmole;

	public GetValueJNRPEPlugin(JMole jmole) {
		this.jmole = jmole;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.jnrpe.plugins.IPluginInterface#execute(it.jnrpe.ICommandLine)
	 */
	public ReturnValue execute(ICommandLine cl) {
		
		String category = cl.getOptionValue('c');
		String name =  cl.getOptionValue('n');
		String attribute =  cl.getOptionValue('a');
		String warningThreshold =  cl.getOptionValue('w');
		String criticalThreshold =  cl.getOptionValue('e');
		if (category == null || name == null || attribute == null) {
			LOG.log(Level.SEVERE, "Null values not allowed: Category: " + category + ", name: " + name + ", attribute: " + attribute);
			return new ReturnValue(Status.UNKNOWN, "UNKNOWN - Missing parameters");
		}
		LOG.log(Level.FINE, "Category: " + category + ", name: " + name + ", attribute: " + attribute + ", warning: " + warningThreshold + ", critical: " + criticalThreshold);
		
		Object value;
		try {
			value = jmole.collectMeasurement(category, name, attribute);
		} catch (Exception e) {
			LOG.log(Level.SEVERE, e.getMessage(), e);
			return new ReturnValue(Status.UNKNOWN, "UNKNOWN - " + e.getMessage()); 
		}
		LOG.log(Level.FINE, "Value=" + value);
		
		String measurement = category + "/" + name + "/" + attribute;
		
		if (value == null) {
			return new ReturnValue(Status.UNKNOWN, "UNKNOWN - Measurement " + measurement + " not found");
		}
		
		// Threshold checking
		try {
			long lValue = Long.parseLong(value.toString());
			if (criticalThreshold != null) {
				if (ThresholdUtil.isValueInRange(criticalThreshold, lValue)) {
					LOG.log(Level.FINE, "Value is critical");					
					return new ReturnValue(Status.CRITICAL,
							"CRITICAL - " + measurement + "=" + value);
				}
			}
			if (warningThreshold != null) {		
				if (ThresholdUtil.isValueInRange(warningThreshold, lValue)) {
					LOG.log(Level.FINE, "Value is warning");
					return new ReturnValue(Status.WARNING,
							"WARNING - " + measurement + "=" + value);
				}
			}
		} catch (BadThresholdException e) {
			LOG.log(Level.SEVERE, e.getMessage(), e);
			return new ReturnValue(Status.UNKNOWN, "UNKNOWN - " + e.getMessage()); 			
		}
		
		// OK
		LOG.log(Level.FINE, "Returning value: " + value);
		return new ReturnValue(Status.OK, "OK - " + measurement + "=" + value);			
	}
	
}
