package net.welen.jmole.protocols.nrpe;

import net.welen.jmole.protocols.Protocol;

public interface NRPEMBean extends Protocol {
	
	public String getAddress();
	public void setAddress(String address);
	public Integer getPort();
	public void setPort(Integer port);
	public String[] getAcceptedHosts();
	public void setAcceptedHosts(String acceptedHosts[]);
	public Boolean isUseSSL();
	public void setUseSSL(Boolean useSSL);
	
}
