package net.welen.jmole.protocols.nrpe;

import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.welen.jmole.JMole;
import it.jnrpe.ICommandLine;
import it.jnrpe.ReturnValue;
import it.jnrpe.Status;
import it.jnrpe.plugins.IPluginInterface;

public class StatusJNRPEPlugin implements IPluginInterface {

	private final static Logger LOG = Logger.getLogger(NRPE.class.getName());
	
	private JMole jmole;
	
	private boolean warnings = true;
	
	public StatusJNRPEPlugin(JMole jmole, boolean warnings) {
		this.jmole = jmole;
		this.warnings = warnings;
	}
	
	public ReturnValue execute(ICommandLine commandLine) {
									
		try {
			
			if (!warnings) {
				// Critical?
				Map<String, Map<String, String>> critical = jmole.criticalMessages();
	
				if (critical.size() > 0) {
					LOG.log(Level.FINE, "Returning: CRITICAL " + critical);
					return new ReturnValue(Status.CRITICAL, critical.toString());
				}
			} else {	
				// Warnings
				Map<String, Map<String, String>> warnings = jmole.warningMessages();
	
				if (warnings.size() > 0) {
					LOG.log(Level.FINE, "Returning: WARNINGS " + warnings);
					return new ReturnValue(Status.WARNING, warnings.toString());
				}
			}
			
			LOG.log(Level.FINE, "Returning: OK ");			
			return new ReturnValue(Status.OK, "OK - " + "No problems detected.");
			
		} catch (Exception e) {
			LOG.log(Level.SEVERE, e.getMessage(), e);
			return new ReturnValue(Status.UNKNOWN, "");
		}
	}
		
}
