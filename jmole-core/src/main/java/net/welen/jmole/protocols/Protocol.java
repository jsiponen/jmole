package net.welen.jmole.protocols;

import net.welen.jmole.JMole;

public interface Protocol {
	
	/**
	 * Is the protocol enabled?
	 */
	public boolean isEnabled();
	
	/**
	 * Callback for starting the protocol, if needed
	 * 
	 * @throws Exception
	 */
	public void startProtocol(JMole jmole) throws Exception;

	/**
	 * Callback for stopping the protocol, if needed
	 * 
	 * @throws Exception
	 */
	public void stopProtocol() throws Exception;
		
}
