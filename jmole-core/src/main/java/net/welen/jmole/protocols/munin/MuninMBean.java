package net.welen.jmole.protocols.munin;

import net.welen.jmole.protocols.Protocol;

public interface MuninMBean extends Protocol {
	
	public String getName();

	public void setName(String name);

	public String getAddress();

	public void setAddress(String address);

	public Integer getPort();

	public void setPort(Integer port);

	public Integer getTcpReadTimeOut();

	public void setTcpReadTimeOut(Integer timeout);

	public Integer getMaxThreads();

	public void setMaxThreads(Integer maxThreads);

	public Integer getCurrentThreads();
	
}
