package net.welen.jmole;

import java.lang.management.ManagementFactory;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.management.AttributeNotFoundException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.MBeanServer;
import javax.management.MBeanServerFactory;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.ReflectionException;

public class Utils {
	
    private final static Logger LOG = Logger.getLogger(Utils.class.getName());

	private static String PROPERTY_MBEAN_SERVER = "jmole.mbeanserver.agentid";

	public static MBeanServer getMBeanServer() {
		String agentId = System.getProperty(PROPERTY_MBEAN_SERVER);
		 
		if (agentId == null) {
			return ManagementFactory.getPlatformMBeanServer();
		}
		
		ArrayList<MBeanServer> servers = MBeanServerFactory.findMBeanServer(null);
		for (MBeanServer server : servers) {
			try {
				if (server.getAttribute(new ObjectName("JMImplementation:type=MBeanServerDelegate"), "MBeanServerId").toString().matches(agentId)) {
					return server;
				}
			} catch (Exception e) {
				LOG.log(Level.SEVERE, "Can't get the AgentId", e);
			}
		}

		List<String> agentIds = new ArrayList<String>();			
		for (MBeanServer server : servers) {
			try {
				agentIds.add(server.getAttribute(new ObjectName("JMImplementation:type=MBeanServerDelegate"), "MBeanServerId").toString());
			} catch (Exception e) {
				LOG.log(Level.SEVERE, "Can't get the AgentId", e);
			}
		}			
		throw new IllegalArgumentException("MBean server with id: " + agentId + " not found. Available id: " + agentIds);
	}
	
}
