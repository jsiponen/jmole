package net.welen.jmole.threshold;

public class ThresholdValues {
	private String warningLowThreshold = "";
	private String warningHighThreshold = "";
	private String criticalLowThreshold = "";
	private String criticalHighThreshold = "";
	
	public String getWarningLowThreshold() {
		return warningLowThreshold;
	}
	public void setWarningLowThreshold(String warningLowThreshold) {
		this.warningLowThreshold = warningLowThreshold;
	}
	public String getWarningHighThreshold() {
		return warningHighThreshold;
	}
	public void setWarningHighThreshold(String warningHighThreshold) {
		this.warningHighThreshold = warningHighThreshold;
	}
	public String getCriticalLowThreshold() {
		return criticalLowThreshold;
	}
	public void setCriticalLowThreshold(String criticalLowThreshold) {
		this.criticalLowThreshold = criticalLowThreshold;
	}
	public String getCriticalHighThreshold() {
		return criticalHighThreshold;
	}
	public void setCriticalHighThreshold(String criticalHighThreshold) {
		this.criticalHighThreshold = criticalHighThreshold;
	}
	
}
