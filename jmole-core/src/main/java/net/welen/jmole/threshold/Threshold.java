package net.welen.jmole.threshold;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Map;
import java.util.HashMap;
import java.util.Stack;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.management.InstanceNotFoundException;
import javax.management.ObjectName;
import javax.management.ReflectionException;

import net.welen.jmole.collector.MBeanCollector;
import net.welen.jmole.finder.MBeanFinder;

public class Threshold implements Runnable {
	
	private final static Logger LOG = Logger.getLogger(Threshold.class.getName());
		
	private boolean stopped = false;
	private int interval = 60000;
	
	private ThresholdValues thresholdValues = new ThresholdValues();
	private Map<String, ThresholdValues> individualThresholdValues = new HashMap<String, ThresholdValues> ();
	
	private Map<ObjectName, String> warningMessages = new HashMap<ObjectName, String>();
	private Map<ObjectName, String> criticalMessages = new HashMap<ObjectName, String>();

	private MBeanFinder mBeanFinder;
	private MBeanCollector mBeanCollector;
	private String attribute;
	
	@Override
	public void run() {
		stopped = false;
		while (!stopped) {		
			LOG.log(Level.FINE, "Running threshold check");

			Map<ObjectName, String> newWarningMessages = new HashMap<ObjectName, String>();
			Map<ObjectName, String> newCriticalMessages = new HashMap<ObjectName, String>();

			for (ObjectName objectName : mBeanFinder.getMatchingObjectNames()) {
				try {
					Object valueObject = mBeanCollector.getValues(objectName).get(attribute);
					if (valueObject == null) {
						LOG.log(Level.FINE, "Data collection returned null. Skipping it as it is probably not calculated yet");
						continue;
					}
					
					Double value = Double.parseDouble(valueObject.toString());
					ThresholdValues individualThresholdValue = individualThresholdValues.get(attribute);
					String low;
					String high;
					
					// Warning
					if (individualThresholdValue == null) {
						low = getWarningLowThreshold();	
						high = getWarningHighThreshold();
					} else {
						low = individualThresholdValue.getWarningLowThreshold();
						high = individualThresholdValue.getWarningHighThreshold();
					}
					low = calculateThreshold(low, mBeanCollector, objectName, attribute);
					high = calculateThreshold(high, mBeanCollector, objectName, attribute);
					
					if (!low.isEmpty() && value < Double.parseDouble(low)) {
						newWarningMessages.put(objectName, attribute + ": " + value + " < " + low);
					}
					if (!high.isEmpty() && value > Double.parseDouble(high)) {
						newWarningMessages.put(objectName, attribute + ": " + value + " > " + high);
					}

					// Critical					
					if (individualThresholdValue == null) {
						low = getCriticalLowThreshold();	
						high = getCriticalHighThreshold();
					} else {
						low = individualThresholdValue.getCriticalLowThreshold();
						high = individualThresholdValue.getCriticalHighThreshold();
					}
					low = calculateThreshold(low, mBeanCollector, objectName, attribute);
					high = calculateThreshold(high, mBeanCollector, objectName, attribute);
					
					if (!low.isEmpty() && value < Double.parseDouble(low)) {
						newCriticalMessages.put(objectName, attribute + ": " + value + " < " + low);
					}
					if (!high.isEmpty() && value > Double.parseDouble(high)) {
						newCriticalMessages.put(objectName, attribute + ": " + value + " > " + high);
					}
					
				} catch (Exception e) {
					LOG.log(Level.SEVERE, e.getMessage(), e);
				}
			}
			warningMessages = newWarningMessages;
			criticalMessages = newCriticalMessages;
			
			try {
				Thread.sleep(interval);
			} catch (InterruptedException e) {
				LOG.log(Level.WARNING, e.getMessage(), e);
			}
		}			
		LOG.log(Level.FINE, "Thread stopped");
	}
	
	public void stopThread() {
		LOG.log(Level.FINE, "Stopping thread");
		stopped = true;
	}

	public String getCriticalLowThreshold() {
		return thresholdValues.getCriticalLowThreshold();
	}

	public void setCriticalLowThreshold(String criticalLowThreshold) {
		this.thresholdValues.setCriticalLowThreshold(criticalLowThreshold);
	}

	public String getCriticalHighThreshold() {
		return thresholdValues.getCriticalHighThreshold();
	}

	public void setCriticalHighThreshold(String criticalHighThreshold) {
		this.thresholdValues.setCriticalHighThreshold(criticalHighThreshold);
	}		

	public String getWarningLowThreshold() {
		return thresholdValues.getWarningLowThreshold();
	}

	public void setWarningLowThreshold(String warningLowThreshold) {
		this.thresholdValues.setWarningLowThreshold(warningLowThreshold);
	}

	public String getWarningHighThreshold() {
		return thresholdValues.getWarningHighThreshold();
	}

	public void setWarningHighThreshold(String warningHighThreshold) {
		this.thresholdValues.setWarningHighThreshold(warningHighThreshold);
	}		

	public void setIndividualThresholds(Map<String, ThresholdValues> individualThresholdValues) {
		this.individualThresholdValues = individualThresholdValues;
	}

	public Map<String, ThresholdValues> getIndividualThresholds() {
		return individualThresholdValues;
	}
	
	public int getInterval() {
		return interval;
	}

	public void setInterval(int interval) {
		this.interval = interval;
	}

	public Map<ObjectName, String> getWarningMessages() {
		return warningMessages;
	}

	public void setWarningMessages(Map<ObjectName, String> warningMessages) {
		this.warningMessages = warningMessages;
	}

	public Map<ObjectName, String> getCriticalMessages() {
		return criticalMessages;
	}

	public void setCriticalMessages(Map<ObjectName, String> criticalMessages) {
		this.criticalMessages = criticalMessages;
	}
	
	public void setMBeanFinder(MBeanFinder mBeanFinder) {
		this.mBeanFinder = mBeanFinder;
	}
	
	public void setMBeanCollector(MBeanCollector mBeanCollector) {
		this.mBeanCollector = mBeanCollector;
	}

	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}

	static public String calculateThreshold(String thresholdString, MBeanCollector mbeanCollector, ObjectName objectName, String attribute) throws InstanceNotFoundException, ReflectionException {		
		String data = thresholdString;
		Map<String, Object> values = mbeanCollector.getValues(objectName);
		LOG.log(Level.FINE, "Before variable replacement: " + data);
		for (String attributeName : mbeanCollector.getAttributes()) {			
			if (values.containsKey(attributeName) && values.get(attributeName) != null) {
				String tmpSplit[] = data.split(",");
				for (int i=0; i<tmpSplit.length; i++) {
					if (tmpSplit[i].equals(attributeName)) {
						tmpSplit[i] = values.get(attributeName).toString();
					}
				}
				StringBuffer tmp = new StringBuffer();
				for (String part : tmpSplit) {
					if (tmp.length() > 0) {
						tmp.append(",");
					}
					tmp.append(part);
				}
				data = tmp.toString();
			}
		}			
		LOG.log(Level.FINE, "After variable replacement: " + data);
		return rpnCalculate(data);
	}
	
	static private String rpnCalculate(String expr) {		
		if (expr == null || expr.isEmpty()) {
			return "";
		}
		Stack<Double> stack = new Stack<Double>();
		String pieces[] = expr.split(",");
		for (int i = 0; i < pieces.length; i++) {
			String value = pieces[i].trim();
			if (value.equals("+")) {
				Double lastValue = stack.pop();
				Double lastLastValue = stack.pop();
				stack.push(lastLastValue + lastValue);
			} else if (value.equals("-")) {
				Double lastValue = stack.pop();
				Double lastLastValue = stack.pop();
				stack.push(lastLastValue - lastValue);
			} else if (value.equals("*")) {
				Double lastValue = stack.pop();
				Double lastLastValue = stack.pop();
				stack.push(lastLastValue * lastValue);
			} else if (value.equals("/")) {
				Double lastValue = stack.pop();
				Double lastLastValue = stack.pop();
				stack.push(lastLastValue / lastValue);
			} else {
				stack.push(Double.valueOf(pieces[i]));
			}
		}
		double answer = stack.pop();
					
		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
		symbols.setDecimalSeparator('.');
		DecimalFormat formatter = new DecimalFormat("#.#", symbols); 
											
		return formatter.format(answer);
	}

}
