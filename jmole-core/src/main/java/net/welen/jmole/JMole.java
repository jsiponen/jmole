package net.welen.jmole;

import java.util.Map;
import java.util.HashMap;

import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.InstanceAlreadyExistsException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanRegistrationException;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectName;
import javax.management.Notification;
import javax.management.NotificationListener;
import javax.management.MBeanServerNotification;
import javax.management.relation.MBeanServerNotificationFilter;
import javax.management.MBeanServerDelegate;
import javax.management.ListenerNotFoundException;
import javax.management.MBeanException;
import javax.management.AttributeNotFoundException;
import javax.management.ReflectionException;
import javax.management.IntrospectionException;

import com.google.gson.Gson;

import net.welen.jmole.threshold.Threshold;

import java.util.List;
import java.util.Collections;
import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.beans.XMLDecoder;
import java.beans.ExceptionListener;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class JMole implements NotificationListener, JMoleMBean {

    private final static Logger LOG = Logger.getLogger(JMole.class.getName());

	static public String OBJECT_NAME = "net.welen.jmole:service=jmole";
	static public String CONFIG_FILENAMES_PROPERTY = "jmole.config.filenames";
	static public String CONFIG_LEVEL_PROPERTY = "jmole.config.level";

	private MBeanServer server = Utils.getMBeanServer();
	private List<Configuration> configurations = new ArrayList<Configuration>();
	
	public void register() throws MalformedObjectNameException,
			InstanceAlreadyExistsException, MBeanRegistrationException,
			NotCompliantMBeanException, InstanceNotFoundException {
		LOG.log(Level.FINE, "Registering JMole MBean");
		server.registerMBean(this, new ObjectName(OBJECT_NAME));
		MBeanServerNotificationFilter filter = new MBeanServerNotificationFilter();
		filter.enableAllObjectNames();
		try {
			server.addNotificationListener(MBeanServerDelegate.DELEGATE_NAME, this, filter, null);
		} catch (InstanceNotFoundException e) {
			LOG.log(Level.SEVERE, "Failed registering the MBean notification listener", e);
			server.unregisterMBean(new ObjectName(OBJECT_NAME));
			throw e;
		}
	}

	public void unregister() throws MalformedObjectNameException,
			InstanceNotFoundException, MBeanRegistrationException,
			ListenerNotFoundException {
		LOG.log(Level.FINE, "Removing JMole MBean");
		deactivateThresholds();
		try {
			server.removeNotificationListener(MBeanServerDelegate.DELEGATE_NAME, this);
		} finally {
			server.unregisterMBean(new ObjectName(OBJECT_NAME));
		}
	}

	public synchronized void configure() throws MalformedObjectNameException,
			FileNotFoundException, MBeanException, AttributeNotFoundException,
			InstanceNotFoundException, ReflectionException,
			IntrospectionException {				
		
		List<Configuration> newConfigurations = Collections.synchronizedList(new ArrayList<Configuration>());

		String fileNames = "JMole_JVM.xml";
		if (System.getProperty(CONFIG_FILENAMES_PROPERTY) != null) {
			fileNames = System.getProperty(CONFIG_FILENAMES_PROPERTY);
		}

		for (String fileName : fileNames.split("\\|")) {
			LOG.log(Level.INFO, "Configuring JMole with config file: " + fileName);

			XMLDecoder decoder = null;
			try {
				class SimpleExceptionListener implements ExceptionListener {
					private Exception e = null;
	
					public void exceptionThrown(Exception e) {
						this.e = e;
					}
	
					Exception getException() {
						return e;
					}
				}
	
				SimpleExceptionListener myListener = new SimpleExceptionListener();
				
				if (new File(fileName).exists()) {
					decoder = new XMLDecoder(new BufferedInputStream(new FileInputStream(fileName)));
				} else {
					decoder = new XMLDecoder(Thread.currentThread().getContextClassLoader().getResourceAsStream(fileName));
				}
	
				newConfigurations.addAll((List<Configuration>) decoder.readObject());
	
				Exception exception = myListener.getException();
				if (exception != null) {
					throw new IllegalStateException(exception);
				}
			} finally {
				if (decoder != null) {
					decoder.close();
				}
			}
		}

		// Remove all configurations that don't meet the level criteria
		int level = Integer.getInteger(CONFIG_LEVEL_PROPERTY, 3);
		if (level < 1 || level > 5) {
			throw new RuntimeException(CONFIG_LEVEL_PROPERTY + " must be 1-5");
		}
		List<Configuration> levelFilteredConfigurations = new ArrayList<Configuration>();
		for (Configuration configuration : newConfigurations) {
			if (configuration.getLevel() <= level) {
				levelFilteredConfigurations.add(configuration);			
			}
		}
		
		deactivateThresholds();
		configurations = levelFilteredConfigurations;
		discover();
		activateThresholds();
	}

	public synchronized void discover() throws MBeanException, AttributeNotFoundException, InstanceNotFoundException,
			ReflectionException, IntrospectionException {		
		LOG.log(Level.FINE, "Running JMole discovery");
		for (Configuration configuration : configurations) {
			configuration.getMBeanFinder().updateMatchingObjectNames();
		}
	}

	public List<Configuration> getConfiguration() {
		return configurations;
	}

	public Map<String, List<Map<String, Map<String, Object>>>> collectMeasurements()
			throws InstanceNotFoundException, ReflectionException,
			MBeanException, AttributeNotFoundException {
		
		Map<String, List<Map<String, Map<String, Object>>>> answer = new HashMap<String, List<Map<String, Map<String, Object>>>>();
		for (Configuration configuration : configurations) {
			String category = configuration.getPresentationInformation().getCategory();
			
			if (answer.containsKey(category)) {
				continue;
			}
			
			Map<String, Map<String, Object>> data = collectMeasurements(category);
			
			if (!data.isEmpty()) {
				if (answer.get(category) == null) {
					answer.put(category, new ArrayList<Map<String,Map<String,Object>>>());
				}
				answer.get(category).add(data);
			}
			
		}
		return answer;
	}

	@Override
	public String collectMeasurementsAsJSON()
			throws InstanceNotFoundException, ReflectionException,
			MBeanException, AttributeNotFoundException {		
		return new Gson().toJson(collectMeasurements());
	}

	public Map<String, Map<String, Object>> collectMeasurements(String category)
			throws InstanceNotFoundException, ReflectionException,
			MBeanException, AttributeNotFoundException {
		
		Map<String, Map<String, Object>> answer = new HashMap<String, Map<String, Object>>();
		for (Configuration configuration : configurations) {
			if (configuration.getPresentationInformation().getCategory().equals(category)) {
				for (ObjectName objectName : configuration.getMBeanFinder().getMatchingObjectNames()) {				
					answer.put(configuration.getMBeanCollector().getName(objectName),
							configuration.getMBeanCollector().getValues(objectName));
				}
			}
		}
		return answer;
	}

	@Override
	public String collectMeasurementsAsJSON(String category)
			throws InstanceNotFoundException, ReflectionException,
			MBeanException, AttributeNotFoundException {
		return new Gson().toJson(collectMeasurements(category));
	}

	public Map<String, Object> collectMeasurements(String category, String name)
			throws InstanceNotFoundException, ReflectionException,
			MBeanException, AttributeNotFoundException {
		
		for (Configuration configuration : configurations) {
			if (configuration.getPresentationInformation().getCategory().equals(category)) {
				for (ObjectName objectName : configuration.getMBeanFinder().getMatchingObjectNames()) {
					if (configuration.getMBeanCollector().getName(objectName).equals(name)) {
						return configuration.getMBeanCollector().getValues(objectName);
					}
				}
			}
		}
		return null;
	}

	@Override
	public String collectMeasurementsAsJSON(String category, String name)
			throws InstanceNotFoundException, ReflectionException,
			MBeanException, AttributeNotFoundException {
		return new Gson().toJson(collectMeasurements(category, name));
	}

	
	public Object collectMeasurement(String category, String name, String attribute)
			throws InstanceNotFoundException, ReflectionException,
			MBeanException, AttributeNotFoundException {
				
		return collectMeasurements(category, name).get(attribute);
	}

	public String collectMeasurementAsJSON(String category, String name, String attribute)
			throws InstanceNotFoundException, ReflectionException,
			MBeanException, AttributeNotFoundException {
		return new Gson().toJson(collectMeasurement(category, name, attribute));
	}

	public void handleNotification(Notification notification, Object handback) {
		LOG.log(Level.FINE, "MBean notification recieved", notification);
		if (notification instanceof MBeanServerNotification) {
			if (notification.getType().equals(MBeanServerNotification.REGISTRATION_NOTIFICATION)
					|| notification.getType().equals(MBeanServerNotification.UNREGISTRATION_NOTIFICATION)) {
				LOG.log(Level.FINE, "MBean notication recieved. Reconfiguring");
				try {
					discover();
				} catch (Exception e) {
					LOG.log(Level.SEVERE, "Reconfiguring of JMole failed.", e);
				}
			}
		}
	}

	private void deactivateThresholds() {
		LOG.log(Level.FINE, "Stopping threshold threads");
		
		for (Configuration configuration : configurations) {
			for (Threshold threshold : configuration.getThresholds().values()) {
				threshold.stopThread();
			}
		}
	}
		
	private void activateThresholds() {
		for (Configuration configuration : configurations) {			
			for (Entry<String, Threshold> entry : configuration.getThresholds().entrySet()) {
				String key = entry.getKey();
				Threshold threshold = entry.getValue();
				
				StringBuffer threadName = new StringBuffer("JMole Threshold Thread: " +
						configuration.getPresentationInformation().getCategory() +
						"->" + configuration.getMBeanCollector().getName() +
						"->" + key);
				if (!threshold.getWarningLowThreshold().isEmpty()) {
					threadName.append("  WarningLow");
				}
				if (!threshold.getWarningHighThreshold().isEmpty()) {
					threadName.append("  WarningHigh");
				}
				if (!threshold.getCriticalLowThreshold().isEmpty()) {
					threadName.append("  CriticalLow");
				}
				if (!threshold.getCriticalHighThreshold().isEmpty()) {
					threadName.append("  CriticalHigh");
				}
				
				threshold.setMBeanFinder(configuration.getMBeanFinder());
				threshold.setMBeanCollector(configuration.getMBeanCollector());
				threshold.setAttribute(key);
				
				LOG.log(Level.FINE, "Starting thread: " + threadName);
				new Thread(threshold, threadName.toString()).start();
			}			
		}
	}

	@Override
	public Map<String, Map<String, String>> warningMessages() throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException {
		return constructMessageData(true);
	}

	@Override
	public String warningMessagesAsJSON() throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException {
		return new Gson().toJson(constructMessageData(true));
	}

	@Override
	public Map<String, Map<String, String>> criticalMessages() throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException {
		return constructMessageData(false);
	}

	@Override
	public String criticalMessagesAsJSON() throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException {
		return new Gson().toJson(constructMessageData(false));
	}
	
	private Map<String, Map<String, String>> constructMessageData(boolean warning) throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException {
		Map<String, Map<String, String>> answer = new HashMap<String, Map<String, String>>();
		for (Configuration configuration : configurations) {			
			for (Threshold threshold : configuration.getThresholds().values()) {
				Map<String, String> thresholdMap = new HashMap<String, String>();
				Set<Entry<ObjectName, String>> entrySet = null;
				if (warning) {
					entrySet = threshold.getWarningMessages().entrySet();
				} else {
					entrySet = threshold.getCriticalMessages().entrySet();
				}
				for (Entry<ObjectName, String> entry : entrySet) {
					ObjectName objectName = entry.getKey();
					String msg = entry.getValue();
					thresholdMap.put(configuration.getMBeanCollector().getName(objectName), msg);
				}
				if (!thresholdMap.isEmpty()) {
					answer.put(configuration.getPresentationInformation().getCategory(), thresholdMap);
				}
			}
		}
		return answer;
	}
	
}
