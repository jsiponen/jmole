# JMole - a monitoring framework for Java MBeans

Copyright 2015 Anders Welén, anders@welen.net
(See also "copyright.txt")

For more info and documentation: https://bitbucket.org/awelen/jmole/wiki/Home

## Thirdparty components ##
JMole is internally using the following:

* jnrpe-lib.jar (http://www.jnrpe.it/)
    * Released under "The Apache Software License, Version 2.0"
* google-gson (https://github.com/google/gson)
    * Released under "The Apache Software License, Version 2.0"
